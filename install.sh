#!/bin/bash

STYLEF=routerosv6.xml
LANGF=mikrotik.lang
SYS="/usr/share"
USERH=$HOME
USERI=".local/share"
GTKSV2="/usr/share/gtksourceview-2.0"
GTKSV3="/usr/share/gtksourceview-3.0"
GTKSV4="/usr/share/gtksourceview-4"

if [ "$EUID" != 0 ] && [ "$1" != "u" ]; then
    echo ""
    echo "Usage: ./install.sh [options]"
    echo "You can enter 'u' after the install script to install as a user."
    echo "Otherwise the script must be run as root or with sudo."
    echo ""
    echo "Please be aware that this script will overwrite any pre-existing files."
    echo ""
    echo "Directory locations:"
    echo $GTKSV{2,3,4}
    exit
fi

if [ "$1" = "u" ]; then
    if [ -d "$USERH/$USERI/gtksourceview-3.0" ]; then
    
        echo "Installing to $USERH/$USERI/gtksourceview-3.0."
        
        if [ ! -f "$USERH/$USERI/gtksourceview-3.0/language-specs/$LANGF" ]; then
        
        mkdir "$USERH/$USERI/gtksourceview-3.0/language-specs/"
        cp "$PWD/language-specs/$LANGF" \
        "$USERH/$USERI/gtksourceview-3.0/language-specs"
        
        else
            rm "$USERH/$USERI/gtksourceview-3.0/language-specs/$LANGF"
            cp "$PWD/language-specs/$LANGF" \
            "$USERH/$USERI/gtksourceview-3.0/language-specs/"
        fi
        
        if [ ! -f "$USERH/$USERI/gtksourceview-3.0/styles/$STYLEF" ]; then
        
        mkdir "$USERH/$USERI/gtksourceview-3.0/styles"
        cp "$PWD/styles/$STYLEF" "$USERH/$USERI/gtksourceview-3.0/styles/"
        
        else
            rm "$USERH/$USERI/gtksourceview-3.0/styles/$STYLEF"
            cp "$PWD/styles/$STYLEF" "$USERH/$USERI/gtksourceview-3.0/styles/"
        fi
    fi
else    
    if [ -d "$GTKSV4" ]; then
        echo "Installing to $GTKSV4."
        
        if [ ! -f "$GTKSV4/language-specs/$LANGF" ]; then
        
        cp "$PWD/language-specs/$LANGF" "$GTKSV4/language-specs/"
        
        else
            rm "$GTKSV4/language-specs/$LANGF"
            cp "$PWD/language-specs/$LANGF" "$GTKSV4/language-specs/"
        fi
        
        if [ ! -f "$GTKSV4/styles/$STYLEF" ]; then
        
        cp "$PWD/styles/$STYLEF" "$GTKSV4/styles/"
        
        else
            rm "$GTKSV4/styles/$STYLEF"
            cp "$PWD/styles/$STYLEF" "$GTKSV4/styles/"
        fi
        
    elif [ -d "$GTKSV3" ]; then
        echo "Installing to $GTKSV3."
        
        if [ ! -f "$GTKSV3/language-specs/$LANGF" ]; then
        
        cp "$PWD/language-specs/$LANGF" "$GTKSV3/language-specs/"
        
        else
            rm "$GTKSV3/language-specs/$LANGF"
            cp "$PWD/language-specs/$LANGF" "$GTKSV3/language-specs/"
        fi
        
        if [ ! -f "$GTKSV3/styles/$STYLEF" ]; then
        
        cp "$PWD/styles/$STYLEF" "$GTKSV3/styles/"
        
        else
            rm "$GTKSV3/styles/$STYLEF"
            cp "$PWD/styles/$STYLEF" "$GTKSV3/styles/"
        fi
        
    elif [ -d "$GTKSV2" ]; then
        echo "Installing to $GTKSV2."
        
        if [ ! -f "$GTKSV2/language-specs/$LANGF" ]; then
        
        cp "$PWD/language-specs/$LANGF" "$GTKSV2/language-specs/"
        
        else
            rm "$GTKSV2/language-specs/$LANGF"
            cp "$PWD/language-specs/$LANGF" "$GTKSV2/language-specs/"
        fi
        
        if [ ! -f "$GTKSV2/styles/$STYLEF" ]; then
        
        cp "$PWD/styles/$STYLEF $GTKSV2/styles/"
        
        else
            rm "$GTKSV2/styles/$STYLEF"
            cp "$PWD/styles/$STYLEF" "$GTKSV2/styles/"
        fi

    else
        echo "No gtksourceview directory located under /usr/share.\n"

    fi
    
fi

